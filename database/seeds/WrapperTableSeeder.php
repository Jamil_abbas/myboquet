<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WrapperTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wrappers')->insert(
            [
                'name' => 'Dotted Black white',
                'wrapperImage' => '1546803343.jpg'
            ]
        );

        DB::table('wrappers')->insert(
            [
                'name' => 'colored flowers',
                'wrapperImage' => '1546803679.jpg'
            ]
        );
        DB::table('wrappers')->insert(
            [
                'name' => 'Dotted Colored',
                'wrapperImage' => '1546803706.jpg'
            ]
        );

        DB::table('wrappers')->insert(
            [
                'name' => 'flower',
                'wrapperImage' => '1546803729.jpg'
            ]
        );
    }
}
