<?php

use Illuminate\Database\Seeder;

class BouquetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 1,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '1.jpg',
            ]
        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 2,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '2.jpg',
            ]

        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 3,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '3.jpg',
            ]

        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 1,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '2.jpg',
            ]
        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 2,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '3.jpg',
            ]

        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 3,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '4.jpg',
            ]

        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 1,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '3.jpg',
            ]
        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 2,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '2.jpg',
            ]

        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 3,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '1.jpg',
            ]

        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 1,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '3.jpg',
            ]
        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 2,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '4.jpg',
            ]

        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 3,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '1.jpg',
            ]

        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 1,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '1.jpg',
            ]
        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 2,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '4.jpg',
            ]

        );

        DB::table('bouquets')->insert(
            [
                'name' => 'Some Bouquet',
                'category_id' => 3,
                'description' =>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'price' => '80',
                'bouquetImage' => '2.jpg',
            ]

        );
    }
}
