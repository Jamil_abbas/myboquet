<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/* Admin Routes */
Route::middleware(['auth','admin'])->group(function () {
    Route::get('/admin/dashboard','AdminController@index')->name('admin.dashboard');
    Route::get('/admin/dashboard/flowers','AdminController@flowers')->name('admin.flowers');
    Route::get('/admin/dashboard/wrappers','AdminController@wrappers')->name('admin.wrappers');
    Route::get('/admin/dashboard/categories','AdminController@categories')->name('admin.categories');
    Route::post('/admin/dashboard/bouquet/create','BouquetController@create')->name('addBouquet');
    Route::post('/admin/panel/bouquet/update','BouquetController@updateBouquet')->name('updateBouquet');
    Route::post('/admin/dashboard/flower/create','FlowerController@create')->name('addFlower');
    Route::post('/admin/dashboard/wrapper/create','FlowerController@createWrapper')->name('addWrapper');
    Route::post('/admin/dashboard/categories/create','CategoryController@create')->name('addCategories');
    Route::post('/admin/dashboard/category/delete','CategoryController@delete')->name('deleteCategory');
    Route::post('/admin/dashboard/category/update','CategoryController@update')->name('updateCategory');
    Route::post('/admin/dashboard/bouquet/delete','BouquetController@delete')->name('deleteBouquet');
    Route::post('/admin/dashboard/bouquet/deletebouquet','BouquetController@deleteBouquet')->name('deleteBouquetFromStock');
    Route::post('/admin/dashboard/bouquet/deleteFlower','FlowerController@deleteFlower')->name('deleteFlowerFromStock');
    Route::post('/admin/dashboard/flower/delete','FlowerController@delete')->name('deleteFlower');
    Route::post('/admin/dashboard/wrapper/delete','FlowerController@deleteWrapper')->name('deleteWrapper');
    Route::get('/admin/dashboard/order/all','AdminController@allOrders')->name('admin.orders');
    Route::get('/admin/dashboard/order/pending','AdminController@pendingOrders')->name('admin.pending');
    Route::get('/admin/dashboard/order/dispatched','AdminController@dispatchedOrders')->name('admin.dispatched');
    Route::get('/admin/dashboard/order/delivered','AdminController@deliveredOrders')->name('admin.delivered');
    Route::post('/admin/dashboard/order/status/update','AdminController@updateStatus')->name('updateStatus');
    Route::get('/admin/panel','AdminController@adminPanel')->name('adminDashboard');
    Route::get('/admin/panel/bouquets/all','BouquetController@getAllBouquets')->name('admin.getAllBouquets');
    Route::get('/admin/panel/flowers/all','FlowerController@getAllFlowers')->name('admin.getAllFlowers');
    Route::get('/admin/panel/customers/all','CustomerController@getAllCustomers')->name('admin.getAllCustomers');
    Route::post('/admin/panel/customers/delete','CustomerController@deleteCustomer')->name('admin.deleteCustomer');
    Route::get('/admin/panel/users/all','CustomerController@getAllUsers')->name('admin.allUsers');
    Route::get('/admin/panel/shipment/all','CustomerController@ordersToShip')->name('admin.shipping');
    Route::get('/admin/panel/reports','AdminController@getReports')->name('admin.reports');
    Route::get('/admin/panel/reports/mostordered','AdminController@getMostOrderedProduct')->name('admin.mostOrdered');
});



/* User Routes */
Route::middleware(['user'])->group(function(){
    Route::get('/','HomeController@landing')->name('home');
    Route::get('/show/{id}','BouquetController@show');
    Route::get('/bouquets/all','BouquetController@index')->name('showAllBouquets');
    Route::get('/bouquets/details/{id}','BouquetController@details')->name('details');
    Route::get('/bouquets/cart/{id}','CartController@add')->name('cart');
    Route::get('/bouquets/cart','CartController@show')->name('showCart');
    Route::post('/bouquets/cart/delete/item/{id}','CartController@delete')->name('deleteCartItem');
    Route::post('/bouquets/cart/checkout','CheckoutController@create')->name('createCheckout');
    Route::get('/feedback/{id}','CheckoutController@getFeedback')->name('getFeedback');
    Route::post('/feedback/create','FeedbackController@create')->name('createFeedback');
    Route::post('/bouquets/cart/custom/checkout','CheckoutController@customCheckout')->name('customCheckout');
    Route::post('/bouquets/customize/checkout','CheckoutController@customizeCheckout')->name('createCustomCheckout');
    Route::get('/bouquets/customize','CustomizeBouquetController@index')->name('showCustomize');
    Route::post('/bouquets/customizeDelivery','CustomizeBouquetController@delivery')->name('customDelivery');
    Route::get('/user/order/{email}','OrderStatusController@myOrders')->name('myOrders');
});
