@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <img class="card-img-top" src='/imgs/bouquet.jpg' alt="Card image cap">
                                <div class="card-body"> Some more card content </div>
                                <div class="card-footer">
                                    {{--<button class="btn btn-primary btn-block" data-toggle="modal" data-target="#exampleModal">Add Bouquet</button>--}}
                                    <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#myModal">
                                        Add Bouquet
                                    </button>

                                    <!-- The Modal -->
                                    <div class="modal" id="myModal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">

                                                <!-- Modal Header -->
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Modal Heading</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>

                                                <!-- Modal body -->
                                                <div class="modal-body">
                                                    Modal body..
                                                </div>

                                                <!-- Modal footer -->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <img class="card-img-top" src="/imgs/flower.jpg" alt="Card image cap">
                                <div class="card-body"> Some more card content </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary btn-block">Add Flower</button>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <img class="card-img-top" src="/imgs/placeholder.png" alt="Card image cap">
                                <div class="card-body"> Some more card content </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>
@endsection
