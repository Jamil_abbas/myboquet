@extends('layouts.app')
@section('content')
<div class="jumbotron bg-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 mt-3" style="height: 0.1rem; background-color: black;"></div>
            <h4 class="text-center col-lg-6">CUSTOMIZE YOUR BOUQUET AS EASY AS 1-2-3</h4>
            <div class="col-lg-3 mt-3" style="height: 0.1rem; background-color: black;"></div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-4">
                <h1 class="text-center text-danger font-weight-bold">STEP 1</h1>
                <h3 class="text-center">CHOOSE YOUR FLOWERS</h3>
                <h5 class="text-center text-dark">Select the flowers  you'd like for your bouquet</h5>
            </div>
            <div class="col-lg-4">
                <h1 class="text-center text-danger font-weight-bold">STEP 2</h1>
                <h3 class="text-center">SELECT QUANTITY</h3>

                <h5 class="text-center text-dark">Select the quantity of you flowers you would like for you bouquet</h5>
            </div>
            <div class="col-lg-4">
                <h1 class="text-center text-danger font-weight-bold">STEP 3</h1>
                <h3 class="text-center">SETUP DELIVERY</h3>

                <h5 class="text-center text-dark">You are all done!Just complete this step for setting up delivery</h5>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row m-4 justify-content-center">
            <div class="col-lg-5">
                <a class="btn btn-primary btn-block nav-link">
                    CHOOSE YOUR DESIGN AND QUANTITY
                </a>
            </div>
            <div class="col-lg-5">
                <button onclick="deliver()"  class="btn btn-outline-primary btn-block nav-link" data-toggle="modal" data-target="#customModal">
                    SETUP DELIVERY OF YOUR BOUQUET
                </button>
                <div>

                </div>
            </div>

        </div>
    </div>

    <div class="container">
        <div class="row">
            @if(isset($flowers))
                <div class="card-group">
                @foreach($flowers as $flower)
                    <div class="col-lg-4 mb-2">
                        <div class="card">
                            <img src="/images/flowers/{{$flower->flowerImage}}" alt="" class="card-img-top" style="width: 100%;height: 25vw;object-fit: cover;">
                        </div>
                        <div class="row justify-content-center m-1 mb-4">
                            <div class="btn btn-success mr-2" onclick=decrease({{$flower->id}})>-</div>
                            <input type="text" class="form-control col-lg-4" id="{{$flower->id}}" value="0">
                            <div class="div btn btn-success ml-2" onclick=increase({{$flower->id}})>+</div>
                        </div>
                    </div>
                @endforeach
                </div>
                @endif
                <button onclick="topFunction()" id="myBtn" title="Go to top"
                        style="
                          display: none;
                          position: fixed;
                          bottom: 20px;
                          right: 30px;
                          z-index: 99;
                          font-size: 18px;
                          border: none;
                          outline: none;
                          background-color: red;
                          color: white;
                          cursor: pointer;
                          padding: 15px;
                          border-radius: 50%;
                        "
                >Top</button>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="customModal" tabindex="-1" role="dialog" aria-labelledby="customModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/bouquets/customizeDelivery" method="post" id="form">
                    {{csrf_field()}}
                    <div class="form-group" id="inputs" hidden>
                        <input type="text" value="0" id="itemCount" name="itemCount" hidden/>
                    </div>
                    <p> You have selected <span id="items"></span> items. Please click continue to proceed</p>
                    <input class="btn btn-primary float-right" type="submit" value="Continue" id="submit">
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var flowers = [];
    function increase(id){
        var element = document.getElementById(id);
        var initial = element.value;
        initial = parseInt(initial)
        initial = initial+1;
        element.value = initial;
        var x;
        var isExist = false;
        var key = 'flower_'+id;
        if(flowers.length){

            for(x in flowers){
                if(flowers[x].key === key){
                    console.log(flowers);
                    isExist = true;
                    var qty = parseInt(flowers[x].qty);
                    qty = qty + 1;
                    flowers[x].qty = qty;
                    break;
                }
            }
            if(!isExist){
                console.log('!exist');
                flowers.push({
                    key:key,
                    id:id,
                    qty:initial
                });
                console.log(flowers);
            }
        }else{
            if(!isExist){
                console.log('!exist');
                flowers.push({
                    key:key,
                    id:id,
                    qty:initial
                });
                console.log(flowers);
            }
        }

    }
    function decrease(id){
        var element = document.getElementById(id);
        var initial = element.value;
        initial = parseInt(initial);
        var x;
        var key = 'flower_'+id;
        if(initial>0){
            initial = initial-1;
            element.value = initial;
            if(flowers.length){
                for(x in flowers){
                    if(flowers[x].key === key){
                        console.log(flowers);
                        var qty = parseInt(flowers[x].qty);
                        if(qty !=1){
                            qty = qty - 1;
                            flowers[x].qty = qty;
                        }else{
                            flowers.pop(x);
                            console.log(flowers);
                        }

                        break;
                    }
                }

            }
        }

    }

    function deliver(){
        var i=0;
        var count = 0;
        for(flower in flowers) {
            var mi = document.createElement("input");
            mi.setAttribute('type', 'text');
            mi.setAttribute('value', flowers[flower].key);
            mi.setAttribute('name', 'flower_key'+i);
            mi.setAttribute('class','some');
               var s = new XMLSerializer().serializeToString(mi);
            $('#inputs').append(s);
            var qtyInput = document.createElement("input");
            qtyInput.setAttribute('type', 'text');
            qtyInput.setAttribute('value', flowers[flower].qty);
            qtyInput.setAttribute('name', 'flower_qty'+i);
            qtyInput.setAttribute('class','some');
               var t = new XMLSerializer().serializeToString(qtyInput);
            $('#inputs').append(t);
            var inputId = document.createElement("input");
            inputId.setAttribute('type', 'text');
            inputId.setAttribute('value', flowers[flower].id);
            inputId.setAttribute('name', 'flower_id'+i);
            inputId.setAttribute('class','some');
            var iId = new XMLSerializer().serializeToString(inputId);
            $('#inputs').append(iId);

            count = count +1;
            $('#itemCount').val(count);
            i++;
        }
        $('#items').append(i);
    }
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
</script>
    @endsection