<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MakeMyBouquet') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'MakeMyBouquet') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item mr-2">

                            <a href="{{route('showCustomize')}}" class="btn btn-outline-primary">Customize Your
                                Bouquet</a>
                        </li>
                        <li class="nav-item mr-2">

                            <a href="{{route('showCart')}}" class="btn btn-outline-success">
                                Cart <span
                                        class="badge badge-danger">@if(Auth::user()){{\Cart::session(Auth::user()->id)->getContent()->count()}} @else {{\Cart::session(Session::get('user_id'))->getContent()->count()}}@endif</span>
                            </a>
                        </li>
                    @else
                        @if(Auth::user()->email != 'admin@mybouquet.com')
                            <li class="nav-item mr-2">

                                <a href="{{route('showCustomize')}}" class="btn btn-outline-primary">Customize Your
                                    Bouquet</a>
                            </li>
                            <li class="nav-item mr-2">

                                <a href="{{route('showCart')}}" class="btn btn-outline-success">
                                    Cart <span
                                            class="badge badge-danger">@if(Auth::user()){{\Cart::session(Auth::user()->id)->getContent()->count()}} @else {{\Cart::session(Session::get('user_id'))->getContent()->count()}}@endif</span>
                                </a>
                            </li>
                        @else
                            <li class="nav-item mr-2">
                                <a href="{{route('adminDashboard')}}" class="nav-link">Dashboard</a>
                            </li>
                        @endif
                    @endguest

                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login')}}</a>
                        </li>
                        <li class="nav-item">
                            @if (Route::has('register'))
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            @endif
                        </li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if(Auth::user()->email != 'admin@mybouquet.com')
                                    <a class="dropdown-item" href="{{route('myOrders',Auth::user()->email)}}">
                                        My Orders
                                    </a>
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest

                </ul>
            </div>
        </div>
    </nav>

    <main>
        <div class="container mt-4">
            <div class="row bg-info">
                <div class="col-md-12 text-center justify-content-center">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{route('adminDashboard')}}">HOME</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{route('admin.dashboard')}}">ADD ITEM</a>
                        </li>
                        <li class="nav-item dropdown text-white">
                            <a class="nav-link dropdown-toggle text-white" data-toggle="dropdown" href="#">ORDERS</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{route('admin.orders')}}">All Orders</a>
                                <a class="dropdown-item" href="{{route('admin.pending')}}">Pending Orders</a>
                                <a class="dropdown-item" href="{{route('admin.delivered')}}">Delivered Orders</a>
                                <a class="dropdown-item" href="{{route('admin.dispatched')}}">Dispatched Orders</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown text-white">
                            <a class="nav-link dropdown-toggle text-white" data-toggle="dropdown" href="#">PRODUCT STOCK</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{route('admin.getAllBouquets')}}">Bouquets</a>
                                <a class="dropdown-item" href="{{route('admin.getAllFlowers')}}">Flowers</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{route('admin.categories')}}">MANAGE CATEGORIES</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{route('admin.getAllCustomers')}}">CUSTOMERS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{route('admin.allUsers')}}">USERS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{route('admin.shipping')}}">SHIPPING</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{route('admin.reports')}}">REPORTS</a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        @yield('content')
    </main>
</div>
</body>
</html>
