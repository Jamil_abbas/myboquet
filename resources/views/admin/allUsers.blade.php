@extends("layouts.admin")
@section('content')
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-12 text-center mt-4 mb-6">
                <h1> All Users</h1>
            </div>
        </div>
        <div class="container">
            <table class="table table-bordered">
                <thead class=" bg-dark text-white">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Joined On</th>

                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $us)
                    <tr>
                        {{--<td><img height="100px" width="100px" src="/images/flowers/{{$bq->flowerImage}}" alt="Card image cap"></td>--}}
                        <td>{{$us->name}}</td>
                        {{--                                                <td>{{\App\Category::find($bq->category_id)->name}}</td>--}}
                        <td>{{$us->email}}</td>
                        <td>{{date('m/d/y', strtotime($us->created_at)) }}</td>
                        <td>
                            <div class="btn-group-sm">
                                {{--<a class="btn btn-success  form-control" href="#"--}}
                                {{--data-id="{{$bq->id}}"--}}
                                {{--data-name="{{$bq->name}}"--}}
                                {{--data-image="{{$bq->bouquetImage}}"--}}
                                {{--data-price="{{$bq->price}}"--}}
                                {{--data-quantity="{{$bq->quantity}}"--}}
                                {{--id="updateBouquet" data-name="" data-toggle="modal" data-target="#editModal">Edit</a>--}}
                                @if(Auth::user()->email == $us->email)
                                    <h4>Admin</h4>
                                    @else
                                    <a class="btn btn-danger form-control" href="#" data-id="{{$us->id}}" id="deleteCustomer" data-toggle="modal" data-target="#deleteModal">Delete</a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    </div>

    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Customer?</p>
                    <form action="{{route('admin.deleteCustomer')}}" method="post">
                        {{csrf_field()}}
                        <input type="number" id="customer_id" name="id" value="" hidden/>

                        <div class="row justify-content-end">
                            <div class="col-lg-2">
                                <input type="button" class="btn btn-primary" value="Cancel" data-dismiss="modal"/>
                            </div>
                            <div class="col-lg-4">
                                <input type="submit" class="form-control btn btn-danger" value="Delete">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
        <script>
            $(document).on("click", "#deleteCustomer", function () {
                var id = $(this).data('id');
                $(".modal-body #customer_id").val( id );
            });
            // $(document).on("click", "#updateBouquet", function () {
            //     var id = $(this).data('id');
            //     var name = $(this).data('name');
            //     var price = $(this).data('price');
            //     var bqImage =$(this).data('image');
            //     var quantity =$(this).data('quantity');
            //     $(".modal-body #bouquet_id").val( id );
            //     $(".modal-body #name").val( name );
            //     $(".modal-body #price").val( price );
            //     $(".modal-body #quantity").val( quantity );
            //     // $(".modal-body #bouquetImage").val( bqImage );
            //     $(".modal-body #bqImage").attr("src", `/images/bouquets/${bqImage}`);
            //     // $(".modal-body #price").val( price );
            //
            //
            // });

        </script>
    </div>
@endsection