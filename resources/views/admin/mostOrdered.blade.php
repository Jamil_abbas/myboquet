@extends('layouts.admin')
@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>Most Ordered Products</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="card-deck">
                        <div class="card bg-primary">
                            <div class="card-body text-center">
                                <h2 style=" margin-top:5px;color: white"> Most Ordered Product </h2>
                            </div>
                        </div>
                        <div class="card bg-warning">
                            <div class="card-body text-center">
                                <h2 style=" margin-top:5px;color: white">Product with Reviews</h2>
                            </div>
                        </div>
                        <div class="card bg-success">
                            <div class="card-body text-center">
                                <h2 style=" margin-top:5px;color: white"> Popular Brands</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection