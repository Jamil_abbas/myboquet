@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center mt-4 mb-6">
            <h1> All Orders</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered mt-5">
                <thead class=" bg-dark text-white">
                <tr>
                    <th scope="col">Customer Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Conact</th>
                    <th scope="col">Address</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Qty</th>
                    <th scope="col">Order Status</th>
                </tr>
                </thead>
                <tbody>
                @if($orders->count()>0)
                @foreach($orders as $order)
                <tr>
                    <td>{{$order->name}}</td>
                    <td>{{$order->email}}</td>
                    <td>{{$order->contact}}</td>
                    <td>{{$order->shippingaddress}}</td>
                    <td>{{$order->amount}}</td>
                    <td>{{$order->quantity}}</td>
                    <td>
                        <button type="button" id="updateStatus" class="btn btn-success" data-target="#orderStatusModal" data-id="{{$order->id}}" data-toggle="modal" >{{App\OrderStatus::find($order->order_status_id)->status}}</button>
                    </td>
                </tr>
                @endforeach
                @else
                    <tr class=" text-center">
                        <td colspan="7">There are no items</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="orderStatusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Order Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('updateStatus')}}" method="post">
                {{csrf_field()}}
            <div class="modal-body">
                <input type="text" name="order_id" id="order_id" hidden/>
                <div class="form-group">
                    <select class="form-control" name="orderStatus">
                        @foreach(App\OrderStatus::all() as $status)
                        <option value="{{$status->id}}">{{$status->status}}</option>
                            @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).on("click", "#updateStatus", function () {
        var id = $(this).data('id');
        $(".modal-body #order_id").val( id );
        // As pointed out in comments,
        // it is superfluous to have to manually call the modal.
        // $('#addBookDialog').modal('show');
    });
</script>
    @endsection