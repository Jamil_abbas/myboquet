@extends("layouts.admin")
@section('content')
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-12 text-center mt-4 mb-6">
                <h1> Flowers Stock</h1>
            </div>
        </div>
        <div class="container">
            <table class="table">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    {{--<th>Category</th>--}}
                    <th>Quantity</th>
                    <th>price</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($flowers as $bq)
                    <tr>
                        <td><img height="100px" width="100px" src="/images/flowers/{{$bq->flowerImage}}" alt="Card image cap"></td>
                        <td>{{$bq->name}}</td>
{{--                        <td>{{\App\Category::find($bq->category_id)->name}}</td>--}}
                        <td>{{$bq->quantity}}</td>
                        <td>{{$bq->price}}</td>
                        <td>
                            <div class="btn-group-sm">
                                {{--<a class="btn btn-success  form-control" href="#"--}}
                                   {{--data-id="{{$bq->id}}"--}}
                                   {{--data-name="{{$bq->name}}"--}}
                                   {{--data-image="{{$bq->bouquetImage}}"--}}
                                   {{--data-price="{{$bq->price}}"--}}
                                   {{--data-quantity="{{$bq->quantity}}"--}}
                                   {{--id="updateBouquet" data-name="" data-toggle="modal" data-target="#editModal">Edit</a>--}}
                                <a class="btn btn-danger form-control" href="#" data-id="{{$bq->id}}" id="deleteFlower" data-toggle="modal" data-target="#deleteModal">Delete</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    </div>
    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('updateBouquet')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="number" id="bouquet_id" value="" hidden name="bouquet_id" >
                        </div>
                        <div class="form-group">
                            <label for="name">Bouquet Name</label>
                            <input type="text" class="form-control" id="name" name="name"  value="">
                        </div>
                        <div class="form-group">
                            <label for="categories">Category</label>
                            <select class="form-control" name="category" id="categories">
                                <option value="1">Hand tied</option>
                                <option value="2">Head tied</option>
                                <option value="3">Bridal</option>
                                <option value="4">Basket</option>
                                <option value="5">Pomander</option>
                                <option value="6">Single Stem</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="text" class="form-control"  value=""  name="price" id="price">
                        </div>
                        <div class="form-group">
                            <label for="price">Quantity</label>
                            <input type="number" class="form-control"  value=""  name="quantity" id="quantity">
                        </div>
                        <div class="form-group">
                            <input type="file" name="bouquetImage" id="bouquetImage">
                        </div>
                        <div style="width:100%; margin-top: 10px; text-align: center" >
                            <img height="70px" width="70px" id="bqImage">
                            <h6>Existing Image</h6>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Edit Bouquet</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Flower?</p>
                    <form action="{{route('deleteFlowerFromStock')}}" method="post">
                        {{csrf_field()}}
                        <input type="number" id="flower_id" name="id" value="" hidden/>

                        <div class="row justify-content-end">
                            <div class="col-lg-2">
                                <input type="button" class="btn btn-primary" value="Cancel" data-dismiss="modal"/>
                            </div>
                            <div class="col-lg-4">
                                <input type="submit" class="form-control btn btn-danger" value="Delete">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
        <script>
            $(document).on("click", "#deleteFlower", function () {
                var id = $(this).data('id');
                $(".modal-body #flower_id").val( id );
            });
            // $(document).on("click", "#updateBouquet", function () {
            //     var id = $(this).data('id');
            //     var name = $(this).data('name');
            //     var price = $(this).data('price');
            //     var bqImage =$(this).data('image');
            //     var quantity =$(this).data('quantity');
            //     $(".modal-body #bouquet_id").val( id );
            //     $(".modal-body #name").val( name );
            //     $(".modal-body #price").val( price );
            //     $(".modal-body #quantity").val( quantity );
            //     // $(".modal-body #bouquetImage").val( bqImage );
            //     $(".modal-body #bqImage").attr("src", `/images/bouquets/${bqImage}`);
            //     // $(".modal-body #price").val( price );
            //
            //
            // });

        </script>
    </div>
@endsection