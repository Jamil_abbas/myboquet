@extends('layouts.app')
@section('content')
<div class="container-fluid mt-4">
    <div class="card">
        <div class="card-header text-capitalize bg-primary">
            <h3 class="text-center font-weight-bold text-white ">
                Cart
            </h3>
        </div>
        <div class="card-body">
            <table class="table">

                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Total</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $grantTotal = array();
                $i=0;
                $no=1;
                $sum=0;
                $totalqty=0;

                ?>
                @if($items->count() > 0)
                @foreach($items as $item)
                <tr>
                    <th scope="row">{{$no}}</th>
                    <td>{{$item->name}}</td>
                    <td>{{$item->price}}</td>
                    <td>{{$item->quantity}}</td>
                    <td>{{$grantTotal[$i] = $item->quantity*$item->price}}</td>
                    <td>
                        <form action="{{route('deleteCartItem',$item->id)}}" method="post">
                            {{csrf_field()}}
                            <input class="btn btn-danger" type="submit" value="delete"/>
                        </form>

                    </td>
                </tr>
                    <?php $i=$i+1;$no++ ; $totalqty = $totalqty + $item->quantity ?>
                    @endforeach
                    @else
                <p>There are no items in the cart.</p>
                    @endif
                </tbody>
            </table>
            <div class="row">
                <div class="col col-lg-3"></div>
                <div class="col col-lg-6">
                    <h1 class="text-center btn btn-info btn-block">
                        <div hidden>
                            @foreach($grantTotal as $total)
                                {{$sum = $sum + $total}}
                            @endforeach
                        </div>
                        Total = {{$sum}}
                    </h1>
                </div>
                <div class="col col-lg-3"></div>
            </div>
        </div>
    </div>
    <div class="card mt-5 mb-10">
        <div class="card-header bg-primary">
            <h3 class="text-center font-weight-bold text-white ">
                Checkout
            </h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-3"></div>
                <div class="col-6">
                    <div class="container">
                    <form action="{{route('createCheckout')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="name" class="form-control" id="name" name ="name" value="{{Auth::user() ? Auth::user()->name : ""}}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email address:</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{Auth::user() ? Auth::user()->email : ""}}">
                        </div>
                        <div class="form-group">
                            <label for="contact">Contact#:</label>
                            <input type="number" class="form-control" id="contact" name="contact">
                        </div>

                        <div class="form-group">
                            <label for="amount">Amount:</label>
                            @if(isset($sum))
                            <input type="number" class="form-control" id="amount" name="amount" value="{{$sum }}">
                                @else
                                <input type="number" class="form-control" id="amount" name="amount" value="0">
                                @endif
                        </div>

                        <div class="form-group">
                            <label for="shippingaddress">Shipping Address</label>
                            <textarea class="form-control" id="shippingaddress" name="shippingaddress" rows="4"></textarea>
                        </div>
                            <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key='pk_test_5PMijbI1gCUiWMY1y5E2Q6Hv'
                                    data-amount={{$sum}}
                                    {{--data-name="myBouquet"--}}
                                    data-description="Widget"
                                    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                    data-locale="auto">
                            </script>

                    </form>
                    </div>
                </div>
                <div class="col-3"></div>
            </div>
        </div>
    </div>
</div>
    @endsection