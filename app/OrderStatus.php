<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    function checkout(){
        return $this->belongsTo('App\Checkout');
    }
}
