<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bouquet extends Model
{
    function categories(){
        return $this->hasOne('App\Category');
    }

    function checkout(){
        return $this->belongsTo('App\Checkout');
    }

    function feedback(){
        return $this->hasMany('App/Feedback');
    }

    function customCheckout(){
        return $this->hasMany('App/CustomCheckout');
    }
}
