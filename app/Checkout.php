<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    function bouquet(){
        return $this->hasMany('App\Bouquet');
    }

    function orderStatus(){
        return $this->hasOne('App\OrderStatus');
    }
}
