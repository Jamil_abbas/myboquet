<?php

namespace App\Http\Controllers;

use App\Bouquet;
use App\Feedback;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    function add($id){
        $bouquet = Bouquet::find($id);
        $id = $bouquet->id;
        $name = $bouquet->name;
        $price = $bouquet->price;
        $qty = 1;

        if(Auth::user()){
            $userId = Auth::user()->id;
            \Cart::session($userId)->add($id,$name,$price,$qty);
        }else{
            \Cart::session(Session::get('user_id'))->add($id,$name,$price,$qty);
        }

        $success = 'Successfully added to cart';
        $feedback = Feedback::all()->where('product_id', $id);
//        dd($feedback);
        $i=0;
        $overallFeedback=0;
        if($feedback->count() > 0) {
            for ($i; $i < $feedback->count()-1; $i++) {
                $overallFeedback = $overallFeedback + $feedback[$i]->rating;
            }
        }
        $feedbackScore=0;
        if(!$feedback->isEmpty() && $feedback->count() >0) {
            $feedbackScore = $overallFeedback / $feedback->count();
        }
        return view('details',compact('bouquet','success', 'feedback','feedbackScore'));
    }

    function show(){
        if(Auth::user())
            $items = \Cart::session(Auth::user()->id)->getContent();
        else
            $items = \Cart::session(Session::get('user_id'))->getContent();

        return view('cart',compact('items'));
    }

    function delete($id){
        if(Auth::user()){
            \Cart::session(Auth::user()->id)->remove($id);
            $items = \Cart::session(Auth::user()->id)->getContent();
        }
        else{
            \Cart::session(Session::get('user_id'))->remove($id);
            $items = \Cart::session(Session::get('user_id'))->getContent();
        }

        return view('cart',compact('items'));
    }

}
