<?php

namespace App\Http\Controllers;

use App\Checkout;
use App\CustomCheckout;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderStatusController extends Controller
{

    function myOrders($email){
        $orders = Checkout::all()->where('email',$email);
        $customizedorders = CustomCheckout::all()->where('email',Auth::user()->email);
//        dd($customizedorders);
        return view('myorders',compact('orders','customizedorders'));
    }
}
