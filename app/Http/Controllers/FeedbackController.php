<?php

namespace App\Http\Controllers;

use App\Bouquet;
use App\Category;
use App\Feedback;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
    //
    function create(Request $request)
    {
//        dd($request);
        $feedback = new Feedback();
        $feedback->comment = $request->comment;
        $feedback->rating = $request->rating;
        $feedback->product_id = $request->productId;
        $feedback->user_id = $request->userId;
        $feedback->save();

        return redirect('/');
//        if(!Auth::user()){
//            if(!Session::exists('user_id')){
//                $user_id = rand();
//                Session::put('user_id',$user_id);
//            }
//
//        }
//        $categories = Category::all();
//        $bouquets = Bouquet::all();
//        $categoryName = "All Bouquets";
//        return view('welcome',compact('categories','bouquets','categoryName'));    }
    }
}
