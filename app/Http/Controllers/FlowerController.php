<?php

namespace App\Http\Controllers;

use App\Wrapper;
use Illuminate\Http\Request;
use App\Flower;
use Illuminate\Support\Facades\File;

class FlowerController extends Controller
{
    function create(Request $request){
        $flower = new Flower();
        $flowerImage = $request->file('flowerImage');
        $flowerImageName = time().'.'.$flowerImage->getClientOriginalExtension();
        $flowerImage->move('images/flowers',$flowerImageName);
        $flower->name = $request->name;
        $flower->price = $request->price;
        $flower->quantity = $request->quantity;
        $flower->flowerImage = $flowerImageName;
        $flower->save();
        return redirect('/admin/dashboard/flowers');
    }
    function createWrapper(Request $request){
        $wrp = new Wrapper();
        $wrapperImage = $request->file('wrapperImage');
        $wrapperImageName = time().'.'.$wrapperImage->getClientOriginalExtension();
        $wrapperImage->move('images/wrappers',$wrapperImageName);
        $wrp->name = $request->name;
        $wrp->quantity = $request->quantity;
        $wrp->wrapperImage = $wrapperImageName;
        $wrp->save();
        return redirect('/admin/dashboard/wrappers');
    }

    function delete(Request $request){
        $flower = Flower::find($request->id);
        $image_path = "/images/flowers/".$flower->flowerImage;  // Value is not URL but directory file path
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        $flower->delete();

        return redirect('/admin/dashboard/flowers');
    }
    function getAllFlowers(){
        $flowers = Flower::all();
        return view('admin.allFlowers', compact('flowers'));
    }

    function deleteFlower(Request $request){
        $flower = Flower::find($request->id);
        $image_path = "/images/flowers/".$flower->bouquetImage;  // Value is not URL but directory file path
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        $flower->delete();

        return redirect('/admin/panel/flowers/all');
    }
    function deleteWrapper(Request $request){
        $flower = Wrapper::find($request->id);
        $image_path = "/images/wrappers/".$flower->wrapperImage;  // Value is not URL but directory file path
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        $flower->delete();

        return redirect('/admin/panel/flowers/all');
    }
}
