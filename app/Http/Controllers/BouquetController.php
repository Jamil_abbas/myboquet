<?php

namespace App\Http\Controllers;
use App\Category;
use App\Feedback;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Bouquet;
class BouquetController extends Controller
{
    function index(){
        $bouquets = Bouquet::all();
        $categories = Category::all();
        $categoryName = "All Bouquets";
        return view('welcome',compact('categories','bouquets','categoryName'));
    }
    function create(Request $request){
        $bouquet = new Bouquet();
        $boquetImage = $request->file('bouquetImage');
        $boquetImageName = time().'.'.$boquetImage->getClientOriginalExtension();
        $boquetImage->move('images/bouquets',$boquetImageName);
        $bouquet->name = $request->name;
        $bouquet->category_id = $request->category;
        $bouquet->description = $request->description;
        $bouquet->price = $request->price;
        $bouquet->quantity = $request->quantity;
        $bouquet->bouquetImage = $boquetImageName;
        $bouquet->save();
        return redirect('/admin/dashboard');
    }
    function updateBouquet(Request $request){
//        dd($request);
        $bouquet = Bouquet::find($request->bouquet_id);
        if($request->name != ''){
            $bouquet->name = $request->name;
        }
        if( $request->category!= ''){
            $bouquet->category_id = $request->category;
        }
        if($request->price != ''){
            $bouquet->price = $request->price;
        }
        if( $request->quantity!= ''){
            $bouquet->quantity = $request->quantity;
        }
        if($request->file('bouquetImage')){
            $boquetImage = $request->file('bouquetImage');
            $boquetImageName = time().'.'.$boquetImage->getClientOriginalExtension();
            $boquetImage->move('images/bouquets',$boquetImageName);
            $bouquet->bouquetImage = $boquetImageName;
        }
        $bouquet->save();
        return redirect('/admin/panel/bouquets/all');
    }

    function delete(Request $request){
        $bouquet = Bouquet::find($request->id);
        $image_path = "/images/bouquets/".$bouquet->bouquetImage;  // Value is not URL but directory file path
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        $bouquet->delete();

        return redirect('/admin/dashboard');
    }
    function deleteBouquet(Request $request){
        $bouquet = Bouquet::find($request->id);
        $image_path = "/images/bouquets/".$bouquet->bouquetImage;  // Value is not URL but directory file path
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        $bouquet->delete();

        return redirect('/admin/panel/bouquets/all');
    }

    function show($id){
        $bouquets = Bouquet::all()->where('category_id',$id);
        $categories = Category::all();
        $categoryName = Category::find($id)->name;
        return view('welcome',compact('categories','bouquets','categoryName'));
    }

    function details($id){
        $bouquet = Bouquet::find($id);
        $feedback = Feedback::all()->where('product_id', $id);
//        dd($feedback->count());
//        dd($feedback);
         $i=0;
         $overallFeedback=0;
         if($feedback->count() > 0 ) {
//             for ($i; $i < $feedback->count(); $i++) {
////                 $overallFeedback = $overallFeedback + $feedback[$i]->rating;
//
//             }
             foreach($feedback as $f){
                $overallFeedback = $overallFeedback + $f->rating;
             }
         }
//         dd($overallFeedback);
        $feedbackScore=0;
        if($feedback->count() >0) {
            $feedbackScore = $overallFeedback / $feedback->count();
//            dd($feedbackScore);
        }


        return view('details',compact('bouquet','feedback','feedbackScore'));
    }
    function getAllBouquets(){
        $bouquets = Bouquet::all();
        return view('admin.allbouquets', compact('bouquets'));
    }

}
