<?php

namespace App\Http\Controllers;

use App\Category;
use App\Checkout;
use App\CustomCheckout;
use App\Feedback;
use App\Flower;
use App\OrderStatus;
use App\User;
use App\Wrapper;
use Illuminate\Http\Request;
use App\Bouquet;
use Illuminate\Support\Facades\DB;
use Stripe\Order;

class AdminController extends Controller
{
    function index(){
        $bouquets = Bouquet::all();

        return view('admin.dashboard',compact('bouquets'));
    }
    function flowers(){
        $flowers = Flower::all();
        return view('admin.flowers',compact('flowers'));
    }
    function wrappers(){
        $wrappers = Wrapper::all();
        return view('admin.wrappers', compact('wrappers'));
    }
    function categories(){
        $categories = Category::all();
        return view('admin.categories',compact('categories'));
    }

    function allOrders(){
        $orders = Checkout::all();
        return view('admin.allOrders',compact('orders'));
    }

    function updateStatus(Request $req){
        $checkout = Checkout::find($req->order_id);
        $checkout->order_status_id = $req->orderStatus;
        $checkout->save();
//        return redirect('/admin/dashboard/order/all');
        return redirect()->back();
    }
    function pendingOrders(){
        $orders = Checkout::all()->where('order_status_id',1);
        return view('admin.pendingOrders',compact('orders'));
    }
    function dispatchedOrders(){
        $orders = Checkout::all()->where('order_status_id',2);
        return view('admin.dispatchedOrders',compact('orders'));
    }
    function deliveredOrders(){
        $orders = Checkout::all()->where('order_status_id',3);
        return view('admin.deliveredOrders',compact('orders'));
    }
    function adminPanel(){
        $customers = Checkout::all()->count();
        $customizedOrderCustomers = CustomCheckout::all()->count();
        $orders = Checkout::all()->count();
        $registeredUsers = User::all()->count();
        $flowers = Flower::all()->count();
        $bouquets = Bouquet::all()->count();
        $items = $flowers + $bouquets;
        $totalCustomers = $customers + $customizedOrderCustomers;
        return view('admin.adminPanel', compact('totalCustomers','orders','registeredUsers','items'));
    }
    function getReports(){
        $bouquet = DB::table('checkouts')
            ->select(DB::raw('count(bouquet_id) as bouquet_count, bouquet_id'))
            ->groupBy('bouquet_id')
            ->get();
        $mo = Bouquet::all()->where('id',$bouquet[0]->bouquet_id); //Most Ordered
        $cb = Feedback::all()->where('rating', 5);  // Highest Rated
//        $returingCustoemr = Checkout::all()->where('')
        $fc = DB::table('checkouts')
            ->select(DB::raw('count(email) as email_count, email'))
            ->groupBy('email')
            ->get();
//        dd($fc[0]->email);
        $rb = User::all()->where('email', $fc[0]->email);
//        dd($rb);
        return view('admin.reports', compact('mo','cb', 'rb'));
    }
    function getMostOrderedProduct(){
        $ms = DB::table('checkouts')->select('bouquet',count('bouquet_id'))->groupBy('bouquet_id')->orderByDesc();
        return view('admin.mostOrdered');
    }
}
